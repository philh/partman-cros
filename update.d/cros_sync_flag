#!/bin/sh

# This script removes the chromeos_kernel flag for partitions whose
# method is not cros and sets it for partition whose method is cros.

. /lib/partman/lib/base.sh

dev=$1
num=$2
id=$3
size=$4
type=$5
fs=$6
path=$7

cd $dev

if [ $fs = free ]; then
	exit 0
fi

method=
if [ -f $id/method ]; then
	method=$(cat $id/method)
fi

has_cros=no
flags=''
open_dialog GET_FLAGS $id
while { read_line flag; [ "$flag" ]; }; do
	if [ "$flag" != chromeos_kernel ]; then
		flags="${flags:+$flags$NL}$flag"
	else
		has_cros=yes
	fi
done
close_dialog

if [ "$method" = cros ] && [ "$has_cros" = no ]; then
	open_dialog SET_FLAGS $id
	write_line "$flags"
	write_line chromeos_kernel
	write_line NO_MORE
	close_dialog
elif [ "$method" != cros ] && [ "$has_cros" = yes ]; then
	open_dialog SET_FLAGS $id
	write_line "$flags"
	write_line NO_MORE
	close_dialog
fi


# The following is a hack and will be removed in future versions of
# partman. It ensures that the chromeos kernel partition can not be
# used for some regular file system.
if [ "$method" = cros ]; then
	# We want to use the existing file system in the partition
	rm -f $id/format
	# However no existing file system is detected
	rm -f $id/detected_filesystem
fi
